<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\translation;

class PageController extends Controller
{
    //
    public function get_translation($lang, $page_name) {
        //Menu translation
        $dbmenu = translation::where([['page', '=', 'menu'], ['trlang', '=', $lang]])->get()->toArray();
        $menu = array();
        foreach ($dbmenu as $key) {
            $menu[$key['name']] = $key['content'];
        }
        //x

        //Content translation 
        $dbcontent = translation::where([['page', '=', $page_name], ['trlang', '=', $lang]])->get()->toArray();
        $content = array();
        foreach ($dbcontent as $key) {
            $content[$key['name']] = $key['content'];
        }
        //x
        return array('lang' => $lang, 'menu' => $menu, 'content' => $content);
    }

    public function homepage($lang){
        if (in_array($lang, ['ro', 'ru', 'en'])) {
            $more_data = PageController::get_translation( $lang, 'homepage');
            return view('static.homepage')->with($more_data);
        }else{
            return redirect("/en/home");
        }
    }

    public function contacts($lang){
        if (in_array($lang, ['ro', 'ru', 'en'])) {
            $more_data = PageController::get_translation( $lang, 'contactspage');
            return view('static.contacts')->with($more_data);
        }else{
            return redirect("/en/contacts");
        }
    }

    public function aboutus($lang){
        if (in_array($lang, ['ro', 'ru', 'en'])) {
            $more_data = PageController::get_translation( $lang, 'aboutus');
            return view('static.aboutus')->with($more_data);
        }else{
            return redirect("/en/aboutus");
        }
    }

    public function products($lang){
        if (in_array($lang, ['ro', 'ru', 'en'])) {
            $more_data = PageController::get_translation( $lang, 'productspage');
            return view('static.products')->with($more_data);
        }else{
            return redirect("/en/products");
        }
    }
}
