<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Controller@index');




Auth::routes();


Route::get("/{lang}/home", "PageController@homepage");
Route::get("/{lang}/contacts", "PageController@contacts");
Route::get("/{lang}/aboutus", "PageController@aboutus");
Route::get("/{lang}/products", "PageController@products");
