@php
    use \Illuminate\Support\Facades\Request;
@endphp
<!DOCTYPE HTML>
<html>
<!-- InstanceBegin template="/Templates/english.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>cables.md</title>
    <link href="{{ asset('css/stile.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('uploads/icon.jpg') }}" />
   <link href="{{ asset('css/jquery_menutop.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>

 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
   





    <script type="text/javascript">
        $(function () {
            // set opacity to nill on page load
            $("ul#menu span").css("opacity", "0");
            // on mouse over
            $("ul#menu span").hover(function () {
                // animate opacity to full
                $(this).stop().animate({
                    opacity: 1
                }, 'slow');
            },
            // on mouse out
            function () {
                // animate opacity to nill
                $(this).stop().animate({
                    opacity: 0
                }, 'slow');
            });

        });
    </script>
</head>



<body>
 
    <div class="centrato">
        <div id="header">
            <div class="logo"><a href="homepage.htm"><div class="top"><a id="top"></a></div><img src="/uploads/logo.png" alt="logo Comfra" width="228" style="margin-left: 30px;" /></a></div>
            
            <div class="meniudiv">
                <ul class="meniuul">
                    <li class="meniuli"><a href="/{{$lang}}/">{{$menu['homepagename']}}</a></li>
                    <li class="meniuli"><a href="/{{$lang}}/products">{{$menu['productssolutions']}}</a></li>
                    <!--<li class="meniuli"><a href="/{{$lang}}/contacts">{{$menu['contacts']}}</a></li>-->
                    <li class="meniuli"><a href="/{{$lang}}/aboutus">{{$menu['aboutus']}}</a></li>
                    <li class="meniuli lang @if($lang == 'en') {{'selected'}} @endif"><a href="/en/{{Request::segment(2)}}">EN</a></li>
                    <li class="meniuli lang @if($lang == 'ro') {{'selected'}} @endif"><a href="/ro/{{Request::segment(2)}}">RO</a></li>
                    <li class="meniuli lang @if($lang == 'ru') {{'selected'}} @endif"><a href="/ru/{{Request::segment(2)}}">RU</a></li>
                </ul>
            </div>
        </div>
        <div id="left">
                <img src="{{asset('/uploads/contacts.png')}}" alt="">  
        </div>
     
       
