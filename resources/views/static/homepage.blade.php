@include("layouts.head")

<div class="content">

    <h1>
        <center>{{$content['pagetitle']}}</center>
    </h1>
    <hr style="margin-left:10%;width:80%; background-color:black;border-top: 1px solid black;">

    

    <div style="width: 80%; margin-left: 10%;">
        <center>
        {!!$content['content']!!}
        <img src="{{asset('/uploads/handshake.jpg')}}" alt="">
        </center>
    </div>
</div>

@include('layouts.foot')