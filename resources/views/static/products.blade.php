@include("layouts.head")

<div class="content">

    <h1>
        <center>{{$content['pagetitle']}}</center>
    </h1>
    <hr style="margin-left:10%;width:80%; background-color:black;border-top: 1px solid black;">

    

    <div style="width: 80%; margin-left: 10%;">
        <p><b>{{$content['products']}}</b></p>
            <ul style="margin-left: 15px; list-style: circle;">
                {!!$content['productsarray']!!}
            </ul>
        <img src="{{asset('uploads/products.jpg')}}" alt="">
        <p><b>{{$content['services']}}</b></p>
            <ul style="margin-left: 15px; list-style: circle;">
                {!!$content['servicesarray']!!}
            </ul>
        <img src="{{asset('uploads/services.jpg')}}" style="max-width: 500px;" alt="">
    </div>
</div>

@include('layouts.foot')